<?php

/**
 * @file
 * Reverts Features (and enables new dependencies) when a site is verified.
 */

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_aegir_features_revert_post_provision_verify() {
  if (d()->type != 'site' || !module_exists('features')) {
    return;
  }

  drush_log(dt('Ensuring new dependencies are enabled.'));
  module_load_include('inc', 'features', 'features.admin');
  $features = array_filter(features_get_features(), '_aegir_features_revert_filter');
  provision_backend_invoke(d()->name, 'pm-enable', array_keys($features));

  drush_log(dt('Reverting Features.'));
  provision_backend_invoke(d()->name, 'features-revert-all');
}

/**
 * Helper function that filters out disabled Features.
 */
function _aegir_features_revert_filter($module) {
  return $module->status;
}
